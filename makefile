com=gfortran
ras=f95
files=$(wildcard *.$(ras))
border_problem : $(patsubst %.$(ras), %.o, $(files))
	 $(com) $^ -o $@
%.o : %.$(ras) methods.mod
	 $(com) -c $<
methods.mod : methods.f95
	 $(com) -c $<

