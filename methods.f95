module methods
    implicit none
    contains
    

    function f_z(z, y, x)
        real f_z, x, y, z
        f_z = 3*z - 2*y + 2*sin(x)
    end function f_z

    function f_y(z)
        real f_y, z
        f_y = z
    end function f_y

    function shoot(z1)
        real, parameter :: pi = 4 * atan(1.0_8)
        integer, parameter :: n = 100
        real :: shoot, z1, y(1:n+1), x(1:n+1), z(1:n+1), h
        integer i

        h = pi / n
        y(1) = 1 - z1
        z(1) = z1

        do i = 1, n + 1
            x(i) = (i-1) * h
        enddo

        do i = 1, n
            y(i+1) = y(i) + h*f_y(z(i))
            z(i+1) = z(i) + h*f_z(z(i),y(i),x(i))
        enddo
        
        shoot = z(n + 1) + 1/5 - exp(pi) + 6/5 * exp(2 * pi)
    end function shoot        

    subroutine shooting_method(n, h, y, eps)                !метод стрельбы
        integer n, i
        real :: x(1:n+1), y(1:n+1), z(1:n+1)
        real  h, eps, z1, z1_n

        z1_n = 1
        z1 = 10 

        do i = 1, n + 1
            x(i) = (i-1) * h
        enddo

        do while (abs(z1 - z1_n) >= eps)
            z1 = z1_n
            z1_n = z1 - shoot(z1) * eps/(shoot(z1 + eps) - shoot(z1))
        enddo

        y(1) = 1 - z1
        z(1) = z1

        do i = 1, n
            y(i+1) = y(i) + h*z(i)
            z(i+1) = z(i) + h*f_z(z(i), y(i), x(i))
        enddo
        
    end subroutine shooting_method

    subroutine finite_differences(n, x, y)
        integer n, i
        real :: x(0:n), y(0:n), y1(0:n), y2(0:n), alpha(0:n), beta(0:n), F(0:n)
        real  h, A, B, C
        real, parameter :: pi = 4 * atan(1.0_8)

        do i = 0, n 
            x(i) = (i) * h
        enddo

        y(0) = 0
        y(n) = exp(pi) - (8/5) * exp(2 * pi) - 3/5

        do i = 1, n - 1
            y1(i) = (y(i+1) - y(i-1))/(2*h)
            y2(i) = (y(i+1) - 2*y(i) - y(i-1))/h**2
        enddo

        C = 1 - (3/2)*h; A = 1 + (3/2)*h; B = 2*h - 2

        F(0) = 2*sin(x(0))
        F(n) = 2*sin(x(n))

        alpha(0) = -C/B 
        beta(0) = F(0)/B

        do i = 1, n - 1
            F(i) = 2*sin(x(i))
            alpha(i) = -C/(A*alpha(i-1) + B)
            beta(i) = (F(i) - A*beta(i-1))/(A*alpha(i-1) + B)
        enddo

        
        y(n) = (F(n) - A*beta(n-1))/(A*alpha(n-1) + B)

        do i = n - 1, 0, -1
            y(i) = alpha(i)*y(i+1) + beta(i)
        enddo

    end subroutine finite_differences

end module methods
