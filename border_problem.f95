program border_problem
    use methods
    implicit none
    integer i
    integer, parameter :: n = 100
    real, parameter :: pi = 4 * atan(1.0_8)
    real, parameter :: eps = 1e-5
    real :: x(1:n+1), y(1:n+1), x1(0:n), y1(0:n)
    real  h

    open(2, file = "result.dat") 

    h = pi / n
    do i = 1, n + 1
        x(i) = (i - 1) * h
    enddo

    do i = 0, n 
        x1(i) = (i) * h
    enddo

    write(2, *) "n =", n
    write(2, *) "Шаг =", h
    write(2, *) "eps = ", eps
    write(2, *) "___________________________________________________________________________________________________"


    call shooting_method(n, h, y, eps)
    call finite_differences(n, x1, y1)

    write(2, *) "Метод стрельбы для:","                             &
     ", "Метод конечных разностей для:"
    
    write(2, *) "y'(0) + y(0) = 1","                                " , "y(0) = 0, y(pi) = e^pi - 8/5 e^2pi - 3/5"
    write(2, *) "y'(pi) = -1/5 + e^pi -6/5 e^2pi"
    
    write(2, *) "____________________________________________________________________________________________________"
    do i = 1, n + 1
        write(2, *) "x(i)", x(i),"y(i)", y(i),"|","   ", "x(i)", x1(i-1), "y(i)", y1(i-1) 
    enddo
    
end program border_problem